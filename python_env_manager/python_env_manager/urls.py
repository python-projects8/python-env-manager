"""python_env_manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from env_manager.views import edit_package, env_info, home, package_info

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
    path('env/<int:id>/', env_info),
    path('env/<int:id>/<str:package_name>', package_info),
    path('env/<int:id>/<str:package_name>/<str:version>/<str:command>', edit_package)
]
