import json
from os import environ, strerror
from os.path import expanduser
import subprocess
from sys import version
from django.http import HttpResponse, HttpRequest
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, redirect
from .env_manager import EnvManager
envman = EnvManager()
home_context = {
    "environments": envman.env_info
}

def home(request:HttpRequest):
    return render(request, "home.html", context=home_context)

def env_info(request:HttpRequest, id:int):
    env = envman.env_info[id-1]
    context = {
        "env": env,
        "env_id": id
    }
    return render(request, "env.html", context=context)

def package_info(request:HttpRequest, id:int, package_name):
    env = envman.env_info[id-1]
    versions = envman.get_available_package_versions(package_name)
    try:
        package_content = [pkg for pkg in env["packages"] if pkg["name"] == package_name][0]
    except IndexError:
        return redirect("/")
    context = {
        "env": env,
        "env_id": id,
        "package_name": package_name,
        "versions": json.dumps(versions),
        "current_version": package_content["version"]
    }
    return render(request, "package.html", context=context)

def edit_package(request:HttpRequest, id:int, package_name:str, version:str, command:str):
    if request.method != "POST":
        return "<h1> Method not allowed </h1>", 405

    if command == "replace":
        if not envman.upgrade_package(id, package_name, version):
            return redirect("/")
    elif command == "remove":
        if not envman.uninstall_package(id, package_name):
            return redirect("/")
    
    return redirect(f"/env/{id}/{package_name}")