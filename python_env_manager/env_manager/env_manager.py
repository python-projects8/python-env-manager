import os
import subprocess
import sys
from loguru import logger
from python_env_manager.settings import WHITELISTED_DIRS, SEARCH_DIR
from datetime import date, datetime
import requests

logger.remove()
logger.add("logs/env_manager_{time}.log", level="DEBUG")
# if django_settings.DEBUG:
    #logger.add(sys.stdout, level="DEBUG")
# else:
    #logger.add(sys.stdout, level="WARNING")


class EnvManager():
    def __init__(self):
        logger.debug("Initializing EnvManager")
        self.envs = self.get_python_binary_paths()
        self.env_info = self.get_env_info()

    def get_python_binary_paths(self):
        # Get "find" output to find any "python" binaries
        before = datetime.now()
        logger.debug("Searching for python environments...")
        
        command = [
            "find",  
            f"{SEARCH_DIR}",
            "-executable",
            "-name",
            "python"
        ]
        potential_envs, errs = None, None
        try:
            proc = subprocess.Popen(command, stdout=subprocess.PIPE, 
                                            stderr=subprocess.PIPE)
            potential_envs, errs = proc.communicate()
            potential_envs = potential_envs.decode()
            errs = errs.decode()
        except FileNotFoundError:
            logger.critical("'find' was not found. It must be installed on the system (linux only)")
            exit()
        except Exception as err:
            if potential_envs is None or potential_envs == "":
                logger.error(f"Unknown error while searching for python binaries: {err}")
            else:
                logger.critical(f"Unknown critical error while searching for python binaries: {err}")
                exit()
        after = datetime.now()
        error_count = len(errs.split("\n"))
        env_count = len(potential_envs.split("\n"))
        logger.debug(f"Done searching. found {env_count} potential environments in {(after-before).total_seconds()} seconds with {error_count} errors. Errors are typically permission denies")

        # Filter out any that are not inside a "bin" folder
        potential_envs = [env for env in potential_envs.split("\n") if "bin" in env]

        # Initialize Results
        envs = []

        # Filter out whitelisted directories
        for env in potential_envs:
            for directory in WHITELISTED_DIRS:
                if env.startswith(directory):
                    break
            else:
                envs.append(env)

        # Return results
        return envs

    def get_env_info(self):
        # Initialize Results
        results = []

        # For each env
        for env in self.envs:

            # Add dict to results
            results.append({})

            # Get python interpereter path
            results[-1]["python_path"] = env

            # Get python version
            ## Initialize command
            command = [
                results[-1]["python_path"],
                "--version"
            ]
            try:
                proc = subprocess.Popen(command, stdout=subprocess.PIPE)
                version = proc.communicate()[0].decode()
                version = version.split()[1]
            except FileNotFoundError as err:
                logger.error(err)
                continue
            except IndexError as err:
                logger.error(f"Invalid output from python --version: {err}")
                continue
            except Exception as err:
                logger.error(f"Unexpected Error: {err}")
                continue
            results[-1]["version"] = version

            # Determine if pip is installed
            pip_path = env.split("/")
            pip_path[-1] = "pip"
            pip_path = "/".join(pip_path)
            pip_installed = True
            try:
                open(pip_path, "r").close()
            except IOError or FileNotFoundError:
                    pip_installed = False

            # If pip installed
            if pip_installed:

                # Set pip path
                results[-1]["pip_path"] = pip_path

                # Get packages
                command = [
                    pip_path,
                    "freeze"
                ]
                proc = subprocess.Popen(command, stdout=subprocess.PIPE)
                packages = proc.communicate()[0].decode()
                if "==" not in packages:
                    logger.warning(f"Either no packages were installed or pip freeze returned invalid input: {packages}")
                    continue
                packages = packages.split("\n")
                results[-1]["packages"] = []
                for package in packages:
                    if "==" not in package:
                        name = package
                        version = None
                    else:
                        name, version = package.split("==")
                    results[-1]["packages"].append({
                        "name": name,
                        "version": version
                    })

        # Return results
        return results
    
    def get_available_package_versions(self, package:str) -> dict:
        response = requests.get(f"https://pypi.org/pypi/{package}/json")
        try:
            releases = response.json()["releases"]
        except KeyError:
            logger.error(f"Invalid package name: {package}")
        except Exception:
            logger.error(f"Received invalid response from server. Code: {response.status_code}, Message: {response.text}")
        return releases

    def upgrade_package(self, env_id:int, package_name:str, version:str):
        env = self.env_info[env_id-1]
        if not env.get("pip_path"):
            return False
    
        command = [
            env["pip_path"],
            "install",
            f"{package_name}=={version}"
        ]

        try:
            proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output, err = proc.communicate()
            logger.debug(f"- Upgrade - \nOutput: {output}\nError: {err}")
        except Exception as err:
            return False
        
        self.env_info = self.get_env_info()
        return True

    def uninstall_package(self, env_id:int, package_name:str):
        env = self.env_info[env_id-1]
        if not env.get("pip_path"):
            return False
    
        command = [
            env["pip_path"],
            "uninstall",
            package_name
        ]

        try:
            proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output, err = proc.communicate()
            logger.debug(f"- Uninstall - \nOutput: {output}\nError: {err}")
        except Exception as err:
            return False
        
        self.env_info = self.get_env_info()
        return True

if __name__ == "__main__":
    import json
    envman = EnvManager()
    print(json.dumps(envman.get_env_info()))