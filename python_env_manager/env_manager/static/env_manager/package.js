Vue.config.delimiters = ['[[', ']]'];

var app = new Vue({
    el: "#app",
    data: {
        buttons: {
            //{% for version, value in version.items %}
            
            'btn_{{version}}': {
                text: ({{current_version}} == {{version}}) ? "Uninstall" : "Install"
            }

            //{% endfor %}
        }
    }
})

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

function remove(button, package_name, version){
    var csrftoken = getCookie('csrftoken')
    var old_value = app["btn_" + version].text
    app["btn_" + version].text = `
    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
    <span class="sr-only"></span>
    `;
    $.ajax({
        method: 'POST',
        url: package_name + '/' + version + '/remove',
        headers: {
            'X-CSRFToken': csrftoken
        },
        success: function() {
            alert("Success")
        },
        failture: function() {
            alert("Failure")
        }
    });
    app["btn_" + version].text = old_value
}

function replace(button, package_name, version){
    var csrftoken = getCookie('csrftoken')
    var old_value = button.value
    button.value = `
    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
    <span class="sr-only"></span>
    `;
    $.ajax({
        method: 'POST',
        url: package_name + '/' + version + '/replace',
        headers: {
            'X-CSRFToken': csrftoken
        },
        success: function() {
            alert("Success")
        },
        failture: function() {
            alert("Failure")
        }
    });
    button.value = old_value
}