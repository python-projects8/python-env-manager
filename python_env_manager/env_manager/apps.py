from django.apps import AppConfig


class EnvManagerConfig(AppConfig):
    name = 'env_manager'
